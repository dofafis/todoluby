# Lucas Farias de Oliveira (oliveirafarias.lucas@gmail.com)
# Test for Full Stack - React JS, React Native, Node.JS

# ToDo Luby

A simple to do list with React and styled-components. It saves on localstorage the items you create, 
allowing you to reload the page without losing your list. You can also delete and edit the items on your list

## Install Dependencies

```
npm install
```

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.