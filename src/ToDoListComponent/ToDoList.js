import React from 'react'
import ListItems from '../ListItemsComponent/ListItems'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faTrash } from '@fortawesome/free-solid-svg-icons'
import { LocalStorageKeys } from '../utils/LocalStorageKeys'
import { ToDoListContainer, ToDoHeader, ToDoForm, InputItem, AddItem, OrderItems } from './StyledComponents'

library.add(faTrash)

class ToDoList extends React.Component {
  constructor(props) {
    super(props);
    
    this.state = JSON.parse(localStorage.getItem(LocalStorageKeys.SAVED_TO_DO_LIST));

    if(!this.state || !this.state.items || !this.state.currentItem) {
      this.state = {
        items: [],
        currentItem: {
          text: '',
          key: ''
        }
      }
      localStorage.setItem(LocalStorageKeys.SAVED_TO_DO_LIST, JSON.stringify(this.state))
    }

    this.handleInputItem = this.handleInputItem.bind(this);
    this.addItem = this.addItem.bind(this);
    this.removeItem = this.removeItem.bind(this);
    this.updateItem = this.updateItem.bind(this);
    this.orderItems = this.orderItems.bind(this);
  }

  handleInputItem(e) {
    this.setState({
      currentItem: {
        text: e.target.value,
        key: Date.now()
      }
    }, () => {
      localStorage.setItem(LocalStorageKeys.SAVED_TO_DO_LIST, JSON.stringify(this.state))
    })
  }

  addItem(e) {
    e.preventDefault();
    const newItem = this.state.currentItem;
    if(newItem.text && newItem.text.length >= 5) {
      const newItems = [...this.state.items, newItem];
      const newState = {
        items: newItems,
        currentItem: { 
          text: '',
          key: ''
        }
      }
      this.setState(newState, () => {
        localStorage.setItem(LocalStorageKeys.SAVED_TO_DO_LIST, JSON.stringify(newState))
      })

    }
  }

  removeItem(key) {
    const filteredItems = this.state.items.filter(item => item.key !== key );
    this.setState({
      items: filteredItems
    }, () => {
      localStorage.setItem(LocalStorageKeys.SAVED_TO_DO_LIST, JSON.stringify(this.state))
    })
  }

  updateItem(text, key) {
    if(text.length < 5)
      return
    
    const items = this.state.items;
    items.map(item => {
      if(item.key === key) {
        item.text = text
      }
      return item
    })
    this.setState({
      items: items
    }, () => {
      localStorage.setItem(LocalStorageKeys.SAVED_TO_DO_LIST, JSON.stringify(this.state))
    })
  }

  orderItems() {
    const orderedItems = this.state.items
    orderedItems.sort(( item1, item2 ) => {
      if ( item1.text < item2.text ){
        return -1;
      }
      if ( item1.text > item2.text ){
        return 1;
      }
      return 0;
    })

    this.setState({
      items: orderedItems
    }, () => {
      localStorage.setItem(LocalStorageKeys.SAVED_TO_DO_LIST, JSON.stringify(this.state))
    })
  }

  render() {
    return (
      <ToDoListContainer>

        <ToDoHeader>

          <ToDoForm>

            <InputItem type="text" placeholder="Enter Text"
            value={this.state.currentItem.text}
            onChange={this.handleInputItem} />
            
            <AddItem type="submit" onClick={this.addItem}>Adicionar</AddItem>
            <OrderItems type="button" onClick={this.orderItems}>Ordenar</OrderItems>
          </ToDoForm>

        </ToDoHeader>

        <ListItems
          items={this.state.items}
          removeItem={this.removeItem}
          updateItem={this.updateItem}>
        </ListItems>

      </ToDoListContainer>
    )
  }
}

export default ToDoList;