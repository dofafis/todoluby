import styled from "styled-components"

export const ToDoListContainer = styled.div`
  background-color: #004565;
  min-height: 600px;
  width: 500px;
  margin:  40px auto;
  border-radius: 10px;
`

export const ToDoHeader = styled.header``

export const ToDoForm = styled.form``

export const InputItem = styled.input`
  background-color: rgb(27, 112, 137);
  border: 0;
  width: 200px;
  height: 50px;
  padding: 0 20px;
  margin: 20px;
  font-size: 18px;
  border-radius: 10px;
  color: #fff;
  ::placeholder {
    color: rgba(255, 255, 255, 0.5);
  }
  :focus {
    outline: none;
  }
`

export const AddItem = styled.button`
  margin: 10px;
  height: 50px;
  width: 80px;
  border-radius: 5px;
  border: 0;
  background-color: rgb(202, 156, 24);
  cursor: pointer;
  font-weight: bolder;
  color: #fff;
`

export const OrderItems = styled.button`
  margin: 10px;
  height: 50px;
  width: 80px;
  border-radius: 5px;
  border: 0;
  background-color: rgb(187 66 0);
  cursor: pointer;
  font-weight: bolder;
  color: #fff;
`