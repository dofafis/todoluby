import './App.css';
import React from 'react'
import ToDoList from './ToDoListComponent/ToDoList'
import styled from 'styled-components'

const MainContainer = styled.div`
  width: 100%;
  heigth: 100%;
`

const LogoContainer = styled.div`
  width: fit-content;
  margin: 0 auto;
  text-align: center
`

const LogoLuby = styled.img`
  display: block;
  width: 150px;
  margin: auto;
`

const ToDoLuby = styled.h1`
  color: #fff;
  font-family: sans-serif;
`

class App extends React.Component {
  render() {
    return (
      <MainContainer>
        <LogoContainer>
          <ToDoLuby>TODOLUBY</ToDoLuby>
          <LogoLuby src={process.env.PUBLIC_URL + "/Logo_Luby.png"} alt="Luby Logo" />
        </LogoContainer>
        <ToDoList></ToDoList>
      </MainContainer>
    )
  }
}

export default App;
