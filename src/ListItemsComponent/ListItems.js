import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { ListItemsContainer, ItemContainer, ItemValue, IconContainer } from './StyledComponents'

function ListItems(props) {
    const items = props.items;
    const listItems = items.map(item => {
        return <ListItemsContainer key={item.key}>
            <ItemContainer>
                <ItemValue type="text"
                 id={item.key}
                 value={item.text} 
                 onChange={(e) => {
                     props.updateItem(e.target.value, item.key)
                 }} />
                <IconContainer>
                    <FontAwesomeIcon icon="trash" onClick={ () => props.removeItem(item.key) } />
                </IconContainer>
            </ItemContainer>
        </ListItemsContainer>
    })
    return (
        <div>{listItems}</div>
    )
}

export default ListItems;