import styled from "styled-components"

export const ListItemsContainer = styled.div`
    color: #fff;
    width: 400px;
    background-color: #7fb902;
    height: 50px;
    margin: 20px auto;
    border-radius: 5px;
`
export const ItemContainer = styled.p`
    position: relative;
    padding: 10px;
`
export const ItemValue = styled.input`
    background-color: transparent;
    border: 0;
    color: #fff;
    font-size: 18px;
    :focus {
        outline: none;
    }
`
export const IconContainer = styled.span`
    position: absolute;
    right: 10px;
`